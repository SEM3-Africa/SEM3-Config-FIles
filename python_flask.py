import sys # To be able to exit with a system code in case of an error

from flask import Flask
from flask import Flask, flash, redirect, render_template, request, session, abort
import os

# Pass in name to determine root path, then Flask can find other files easier
app = Flask(__name__)
app.run(debug=True,host='0.0.0.0', port=5000)

@app.route('/')
def index():
    html = """
<!DOCTYPE html>
    <html>
        <head>
            <title>Toto - Africa</title>
        </head>
        <style>
        body{
        animation: mymove 1s infinite;
        }
        @keyframes mymove {
        0%   {background: blue;}
        25% {background: red;}
        50% {background: yellow;}
        75% {background: purple;}
        }
        </style>
        <body style="background-color:blue;display:flex;justify-content:center;">
	<div style="padding-top:80em;">
        <iframe width="-0" height="-0"
          src="https://www.youtube.com/embed/nKVduWf3HmU?autoplay=1">
        </iframe>
        </div>
        <form action="/git_pull/" method="post">
            <input type="submit" name="submit" value="Git pull" onclick="git_pull()" style="height:20em;width:20em;">
        </form>
        </iframe>
        </div>
        <form action="/ansible_merge/" method="post">
            <input type="submit" name="submit" value="Ansible merge configs" onclick="ansible_merge()" style="height:20em;width:20em;">
        </form>
    </body>
</html>
"""
    return html


@app.route("/git_pull/", methods=['POST'])
def contact():
    html = """
    <!DOCTYPE html>
        <html>
            <head>
                <title>Git pulled</title>
            </head>
        <body style="background-color:red;/*! display: flex; *//*! justify-content: center; */">
            <h1 style="/*! align-content: center; */text-align: center;/*! justify-items: center; */padding-top: 15em;">Git pulled success</h1>
        </body>
    </html>
    """
    git_pull()
    return html

@app.route("/ansible_merge/", methods=['POST'])
def contact1():
    html = """
    <!DOCTYPE html>
        <html>
            <head>
                <title>Ansible_merge</title>
            </head>
        <body style="background-color:purple;/*! display: flex; *//*! justify-content: center; */">
            <h1 style="/*! align-content: center; */text-align: center;/*! justify-items: center; */padding-top: 15em;">Ansible_merge_success</h1>
        </body>
    </html>
    """
    ansible_merge()
    return html

def git_pull():
    os.system('git pull')

def ansible_merge():
    os.system("ansible-playbook -i /root/SEM3-Config-FIles/ansible/inventory/inventory /root/SEM3-Config-FIles/ansible/playbooks/jansible.yml")

if __name__ == "__main__":
    app.run(debug=True)


