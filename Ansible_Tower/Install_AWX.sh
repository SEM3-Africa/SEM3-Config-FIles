#install some basic software
apt install -y apt-transport-https ca-certificates curl gnupg2 software-properties-common dirmngr git python-pip

#add stretch-backports
echo "deb http://ftp.debian.org/debian stretch-backports main contrib non-free" > /etc/apt/sources.list.d/stretch-backports.list

#add Docker CE Repository
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -

#update package lists
apt update

#install Ansible and docker
apt install -y ansible -t stretch-backports docker-ce docker-compose

#clone awx
cd ~
git clone https://github.com/ansible/awx

#configure installer
cd ~/awx/installer

#changes parameters
sed -i "s|/tmp/pgdocker|/var/pgdocker|g" inventory
sed -i "s|# use_docker_compose=false|use_docker_compose=true|g" inventory
sed -i "s|#project_data_dir=/var/lib/awx/projects|project_data_dir=/var/awx_projects|g" inventory

#Assign the host_port to 5030 instead of 80
sed -i "s|host_port=80|host_port=5030|g" inventory
#install awx
ansible-playbook install.yml -i inventory