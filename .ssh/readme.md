This section will contain all related information to achiveing a connection to the enviroment via the use of SSH

# KEY COMMANDS

For ease of use i will be placing commands below that will add each of the project members public keys to the MX's users.

## TIM

[Azarion Public SSH Keys](https://gitlab.com/Azarion.keys)

    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC3PzwrGi6g8pEkejYKFlzWPMEYI0wJeYRfkUFU3ndTOcr7duZn4YZvKNwDglN0OVDyK/08veciHSzbZd81rvMnc913FdXV+UaSe1NF2eWHqUhwqhQcY/V+YZ4CFkvwrvutpIlZcSRaumvY8bHInMc9K8oSTlDrW8mPeTALOYybDWgxsi0KAr3QDtjvSBrA9iEoz5luHCkTvyQ2hukPTUfaHsM2xGBPTHKhDEnghpPxRYBLatw9OfrMMLZ9s4H0EPgnm08CW1l7AqGP7PHInEQAQp9f9wPnaJrvdvz/ohn52KLAsodb87/T2iroQ2/h5bwAH2vuWtJ2xU3WkD/9FhXP Tim Lyder Nikolajsen (gitlab.com)
    
## BO

[Bo3535 Public SSH Keys](https://gitlab.com/bo3535.keys)

    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDUsLw6uxybz3e1A0gYoH/ZQpMKyE8u56D0EPO5WxLE/+BRKeQespifWe8OMCUhhtlN0/f9Fg6HL6PV8O8WpXF5EChYDWGXgQz8txf4ZGEnpEJJQXD+dfIxVPptvbcMHPHGxeFZlRQ2t3s6eBIqq9g/bqJwpIvNJZgPYeDN4X0EGa+PTABTHE5WDIfd1yq242nKRc1XHVw8CbxLCz9JPCZbCF9LPQvVEqIzq5hMY/o5YdLhSEpf/nd+XDaQ5GCkDxpLPUwwJljnndjPzc4t0/pt4Z7HfyG/2HNfEykll1krGY6NwenBfBUW0orbUFGQucXA702ulEMycHGFzDAdde4z Bo Mikkelsen (gitlab.com)
    
## KYLE

[Syondi Public SSH Keys](https://gitlab.com/syondi.keys)

    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDGnsbFILzb55108i0na+idUIJG386PCAa1Py11ABXU7vToE13IzS5cWRzsMvUzV18LdyAEDLiJ9kRHodt7QUlGCIbcHLwMfoGO0bNSiC9s+Ra643A0xQEZHLn7u+jH6xIkpdlO9ZfvgL8jhOATF8ZimkoWbenmbX4MdEKueK1lWGec8ABiRoOV4DRK1DNgbbHF75HFGdbyKJDqmgr+dXhT+cq5w3q0YHkhScYR2ONOXG1D2urUSk5+MrCw/qAvPADkHJwSCWubMAc3dvgp3PNfNPOqt/5z+nQXJt7Xscasj9iiStwTBVQDo4EaAyyoZ8c41NjnBErBYPNZo4JPHEeN Kyle Mcshane (gitlab.com)
    
## MARTIN

[Physx82 Public SSH Keys](https://gitlab.com/physx82.keys)

    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDCF9xQ/OiyX3C3rdyUuON8PMKG/c09gvic5YXdnxbGh11QHNQazxNHKTAsMoXzNDvAf+7xOrch0OlYcG7cM829pjKJU+h+rw0U+3RRJ3nQSw4qDIMYSep2g5s7a7n9hEWhmr+TI6U+hXa0n2R9uv8mEyBhkUF3U+71Oz76Zm1nBPK/OyYilEYi4lV2uspYoQdht4Yk8xvTUaFqV6E71tTCcuomtuDusSDPJT/eZEHVMGCMvGYfQ8StpMxnAvHrQ1ljXNmCUrGmMtwIzNfUZ9RsCFihRYZmvsCM0Lo2QzVAkOiCjJfm5S4b1w8u1meAfLzY27kbZKJciCyWfML2b5+t Martin Nielsen (gitlab.com)

## KASPER

[Kasperr Public SSH Keys](https://gitlab.com/kasperr.keys)

    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDBAZmxNqCvh5ImVozUvCeFeMJKF4USWqT1+miPJDkjedNA6n2FOyzEQeOsIhWyxaq/zxPQvlCAK0nFNtwEY45V9kWwKSPcyCgL4+nN8JDKFQORdFhGc0wO5f1oQh/e1Q9gIN/4NlAh/INVlDCANh6Qve8670EaCl32dk6GfKclFixWhe7j7c5Szes3Ouv6BzULcG3T6t/j5C5zyTReN4sKh3gMnJcXBV4+jUZdRAfPBL+DjslLpskyycYiWvozlTALeT0AckjGnyb0+3NPHtxBDU8iYIs1zbulMbw5ePVTv6lJBvawHdkml/oDZ0EWZDnyZoiyNFVzRB1NmCxOrqMt Kasper@DESKTOP-9B5LQJF


